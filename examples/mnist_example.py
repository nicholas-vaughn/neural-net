import numpy as np
import time
import os
import sys
# relative import hack
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
import neural_net as nn  # NOQA: E402


x_train, y_train, x_test, y_test = nn.util.load_mnist()

net = nn.StandardNetwork(2, 784, 300, 10, activation_fn=nn.functions.Relu)
net._layers[-1].activation_fn = nn.functions.Softmax()

start = time.time()
for i in range(100000):
    r = np.random.randint(len(x_train))
    net.gradient_descent(x_train[r:r+1], y_train[r:r+1], 0.1)
    # if i % 10000 == 0:
    #     print(net.cost(x_test, y_test))
end = time.time()
print(f"Time to train (seconds): {end - start}")

correct = 0
for i in range(len(x_test)):
    if np.argmax(net.evaluate(x_test[i])) == np.argmax(y_test[i]):
        correct += 1
print(f"Percent correct: {correct / len(x_test) * 100}%")
