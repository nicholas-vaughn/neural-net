import numpy as np


class AcivationFn:
    def activate(self, input):
        raise NotImplementedError

    def gradient(self, input):
        raise NotImplementedError


class Sigmoid(AcivationFn):
    def activate(self, input):
        self.cache = 1 / (1 + np.exp(-input))
        return self.cache

    def gradient(self, input):
        return self.cache * (1 - self.cache)


class Softmax(AcivationFn):
    def activate(self, input):
        self.cache = np.exp(input)
        sum_ = np.sum(self.cache)
        self.cache /= sum_
        return self.cache

    def gradient(self, input):
        return self.cache * (1 - self.cache)


class Relu(AcivationFn):
    def activate(self, input):
        return np.maximum(0, input)

    def gradient(self, input):
        temp = [0 if x < 0 else 1 for x in input]
        return np.array(temp, copy=False)
