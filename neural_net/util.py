import numpy as np
from urllib import request
import pickle
import gzip
import os

_mnist_filenames = [["training_images", "train-images-idx3-ubyte.gz"],
                    ["test_images", "t10k-images-idx3-ubyte.gz"],
                    ["training_labels", "train-labels-idx1-ubyte.gz"],
                    ["test_labels", "t10k-labels-idx1-ubyte.gz"]]


def vector_transpose(x):
    return np.array(x, ndmin=2, copy=False).T


def download_mnist():  # pragma: no cover
    def encode(label):
        arr = np.zeros(10, dtype=np.float32)
        arr[label] = 1
        return arr

    mnist = {}
    base_url = "http://yann.lecun.com/exdb/mnist/"
    for name in _mnist_filenames:
        request.urlretrieve(base_url + name[1], name[1])
    for name in _mnist_filenames[:2]:
        with gzip.open(name[1], 'rb') as f:
            temp = np.frombuffer(f.read(), np.uint8,
                                 offset=16).reshape(-1, 28*28)
            temp = temp.astype(np.float32) / 255
            mnist[name[0]] = temp

    for name in _mnist_filenames[-2:]:
        with gzip.open(name[1], 'rb') as f:
            temp = np.frombuffer(f.read(), np.uint8, offset=8)
            labels = np.empty(len(temp), dtype=object)
            for idx, label in enumerate(temp):
                labels[idx] = encode(label)
            mnist[name[0]] = labels

    with open("mnist.pkl", 'wb') as f:
        pickle.dump(mnist, f)

    for name in _mnist_filenames:
        os.remove(name[1])


def load_mnist():  # pragma: no cover
    with open("mnist.pkl", 'rb') as f:
        mnist = pickle.load(f)
    return mnist["training_images"], mnist["training_labels"], \
        mnist["test_images"], mnist["test_labels"]
