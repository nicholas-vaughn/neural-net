import numpy as np
from neural_net import util
from neural_net import functions


# TODO fix problem with multidimensional inputs either by flatening, or
# supporting higher dimension weights
# TODO support changing optimizer, i.e. SGD, Adam, Demon
# TODO setup grunt
class NeuralNetwork:
    def __init__(self, dtype=np.float32):
        self.dtype = dtype

    def cost(self, inputs, labels):
        cost = 0
        for input, label in zip(inputs, labels):
            cost += np.mean((self.evaluate(input) - label) ** 2)
        return cost / len(inputs)

    def evaluate(self, input):
        raise NotImplementedError

    def _calc_gradient(self):
        raise NotImplementedError

    def gradient_descent(self, inputs, labels, learn_rate):
        raise NotImplementedError

    def __str__(self):  # pragma: no cover
        s = ""
        for layer in self._layers:
            s += str(layer) + "\n"
        return s


class Layer:
    def __init__(self, dtype):
        self.values = None
        self.dtype = dtype

    def evaluate(self):
        raise NotImplementedError

    def calc_gradient(self):
        raise NotImplementedError

    def scale_gradient(self):
        raise NotImplementedError

    def apply_gradient(self):
        raise NotImplementedError

    def __str__(self):  # pragma: no cover
        return self.__class__.__name__ + ": " + str(self.values)


# Represents a fully connected hidden layer
class HiddenLayer(Layer):
    def __init__(self, input_size, size, dtype, weight_range, activation_fn):
        def random_in_range(*args):
            return (np.random.random_sample() - 0.5) * weight_range

        # init fields and fill weights with random values in [range/2, range/2)
        self.activation_fn = activation_fn()
        weight_init_fn = np.vectorize(random_in_range)
        self.weights = np.fromfunction(weight_init_fn, (size, input_size),
                                       dtype=dtype)
        # running sum of weight gradients
        self.weight_grad_sum = np.zeros((size, input_size), dtype=dtype)
        self.biases = np.zeros(size, dtype)
        # running sum of bias gradients
        self.bias_grad_sum = np.zeros(size, dtype)
        self.unactivated_vals = None
        super().__init__(dtype)

    def evaluate(self, input):
        # stores unactivated values for reuse in gradient calc
        self.unactivated_vals = np.matmul(self.weights, input) + self.biases
        self.values = self.activation_fn.activate(self.unactivated_vals)

    def calc_gradient(self, input, cost_grad):
        # calculates gradients of activation of this layer with respect to
        # bias, activation, and weight
        bias_grad = self.activation_fn.gradient(self.unactivated_vals)
        bias_grad = bias_grad.astype(self.biases.dtype, copy=False)
        activation_grad = self.weights * util.vector_transpose(bias_grad)
        weight_grad = np.outer(bias_grad, input)
        # calculates gradient of cost with respect to bias and weight by chain
        # rule
        bias_grad *= cost_grad
        weight_grad *= util.vector_transpose(cost_grad)
        # adds gradient to running sum
        self.weight_grad_sum += weight_grad
        self.bias_grad_sum += bias_grad
        return activation_grad

    def scale_gradient(self, scale):
        self.weight_grad_sum *= scale
        self.bias_grad_sum *= scale

    def apply_gradient(self):
        self.weights -= self.weight_grad_sum
        self.biases -= self.bias_grad_sum
        # resets sums to 0
        self.weight_grad_sum.fill(0)
        self.bias_grad_sum.fill(0)


class LinearNetwork(NeuralNetwork):
    def __init__(self, dtype):
        super().__init__(dtype)

    def evaluate(self, input):
        prev = np.array(input, dtype=self.dtype, copy=False)
        for layer in self._layers:
            layer.evaluate(prev)
            prev = layer.values
        return prev

    def _calc_gradient(self, input, expected):
        output = self.evaluate(input)
        # calculate gradient of cost with respect to activation of last layer
        cost_grad = 2 * (output - expected) / len(output)
        for i in reversed(range(len(self._layers))):
            prev = self._layers[i - 1].values if i != 0 else input
            activation_grad = self._layers[i].calc_gradient(prev, cost_grad)
            # calculate gradient of cost with respect to activation of the
            # next layer with chain rule
            cost_grad = np.matmul(cost_grad, activation_grad)

    def gradient_descent(self, inputs, labels, learn_rate):
        for input, expected in zip(inputs, labels):
            self._calc_gradient(input, expected)
        for layer in self._layers:
            layer.scale_gradient(learn_rate)
            layer.apply_gradient()


class StandardNetwork(LinearNetwork):
    def __init__(self, num_layers, input_size, layer_size, output_size,
                 dtype=np.float32, weight_range=0.01,
                 activation_fn=functions.Sigmoid):
        super().__init__(dtype)
        self._layers = []
        self._init_layers(num_layers, input_size, layer_size, output_size,
                          weight_range, activation_fn)

    def _init_layers(self, num_layers, input_size, layer_size, output_size,
                     weight_range, activation_fn):
        for i in range(num_layers):
            if i == 1:
                input_size = layer_size
            if i == num_layers - 1:
                layer_size = output_size
            layer = HiddenLayer(input_size, layer_size, self.dtype,
                                weight_range, activation_fn)
            self._layers.append(layer)
