import unittest
import neural_net.networks as nn
import neural_net.functions as functions
import numpy as np


class TestActivation(nn.functions.AcivationFn):
    def activate(self, input):
        return input * 2

    def gradient(self, input):
        return np.full_like(input, 2)


class Test(unittest.TestCase):
    def test_ctor(self):
        layer = nn.HiddenLayer(3, 5, np.float32, 1, functions.Sigmoid)
        self.assertIsNone(layer.values)
        self.assertIsNone(layer.unactivated_vals)
        self.assertIs(layer.dtype, np.float32)
        self.assertEqual(layer.weights.shape, (5, 3))
        self.assertEqual(layer.biases.shape, (5,))

    def test_evaluate(self):
        layer = nn.HiddenLayer(2, 3, np.float32, 1, TestActivation)
        layer.weights = np.array([[1, 2],
                                  [3, 4],
                                  [5, 6]])
        layer.biases = np.array([1, 2, 3])
        expected = np.array([12, 26, 40])
        layer.evaluate([1, 2])
        self.assertTrue(np.array_equal(layer.values, expected))

    def test_calculate_gradient(self):
        layer = nn.HiddenLayer(2, 3, np.float32, 1, TestActivation)
        layer.weights = np.array([[1, 2],
                                  [3, 4],
                                  [5, 6]])
        cost_grad = np.array([1, 2, 3])
        input = np.array([1, 2])
        layer.evaluate(input)
        output = layer.calc_gradient(input, cost_grad)
        self.assertTrue(np.array_equal(layer.bias_grad_sum,
                                       np.array([2, 4, 6])))
        self.assertTrue(np.array_equal(layer.weight_grad_sum,
                                       np.array([[2, 4],
                                                 [4, 8],
                                                 [6, 12]])))
        self.assertTrue(np.array_equal(output,
                                       np.array([[2, 4],
                                                 [6, 8],
                                                 [10, 12]])))

    def test_scale_gradient(self):
        layer = nn.HiddenLayer(2, 3, np.float32, 1, functions.Sigmoid)
        layer.weight_grad_sum = np.array([[1, 2],
                                          [3, 4],
                                          [5, 6]])
        layer.bias_grad_sum = np.array([1, 2, 3])
        layer.scale_gradient(3)
        self.assertTrue(np.array_equal(layer.weight_grad_sum,
                                       np.array([[3, 6],
                                                 [9, 12],
                                                 [15, 18]])))
        self.assertTrue(np.array_equal(layer.bias_grad_sum,
                                       np.array([3, 6, 9])))

    def test_apply_gradient(self):
        layer = nn.HiddenLayer(2, 3, np.float32, 1, functions.Sigmoid)
        layer.weights = np.array([[2, 4],
                                  [6, 8],
                                  [10, 12]])
        layer.weight_grad_sum = np.array([[1, 2],
                                          [3, 4],
                                          [5, 6]])
        layer.biases = np.array([2, 4, 6])
        layer.bias_grad_sum = np.array([1, 2, 3])
        expected_biases = np.array([1, 2, 3])
        expected_weights = np.array([[1, 2],
                                     [3, 4],
                                     [5, 6]])
        layer.apply_gradient()
        self.assertTrue(np.array_equal(layer.bias_grad_sum, [0, 0, 0]))
        self.assertTrue(np.array_equal(layer.weight_grad_sum, [[0, 0],
                                                               [0, 0],
                                                               [0, 0]]))
        self.assertTrue(np.array_equal(layer.weights, expected_weights))
        self.assertTrue(np.array_equal(layer.biases, expected_biases))
