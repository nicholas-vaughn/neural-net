import unittest
import neural_net as nn
import numpy as np


class Test(unittest.TestCase):
    def test_ctor(self):
        net = nn.StandardNetwork(3, 4, 3, 2, dtype=np.float64)
        self.assertEqual(net._layers[0].biases.shape, (3,))
        self.assertEqual(net._layers[0].weights.shape, (3, 4))
        self.assertEqual(net._layers[1].weights.shape, (3, 3))
        self.assertEqual(net._layers[1].biases.shape, (3,))
        self.assertEqual(net._layers[2].biases.shape, (2,))
        self.assertEqual(net._layers[2].weights.shape, (2, 3))
        for layer in net._layers:
            self.assertEqual(layer.dtype, np.float64)
