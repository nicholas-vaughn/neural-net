import unittest
import neural_net.networks as nn
import numpy as np


class TestActivation(nn.functions.AcivationFn):
    def activate(self, input):
        return input * 2

    def gradient(self, input):
        return np.full_like(input, 2)


class Test(unittest.TestCase):
    def test_ctor(self):
        net = nn.LinearNetwork(np.float32)
        self.assertIs(net.dtype, np.float32)

    def test_evaluate(self):
        net = nn.StandardNetwork(2, 2, 2, 1, dtype=np.float64,
                                 activation_fn=TestActivation)
        net._layers[0].weights = np.array([[1, 2],
                                          [3, 4]])
        net._layers[1].weights = np.array([1, 2])
        output = net.evaluate([1, 2])
        self.assertEqual(output.dtype, np.float64)
        self.assertEqual(output.shape, (1,))
        self.assertEqual(output[0], 108)
        self.assertTrue(np.array_equal(net._layers[0].values,
                                       np.array([10, 22])))

    def test_calc_gradient(self):
        net = nn.StandardNetwork(1, 2, 3, 3, np.float32, 1, TestActivation)
        net._layers[0].weights = np.array([[1, 2],
                                          [3, 4],
                                          [5, 6]])
        input = np.array([1, 2])
        # chosen to make calculations easier
        expected = np.array([8.5, 19, 29.5])
        net._calc_gradient(input, expected)
        layer = net._layers[0]
        self.assertTrue(np.array_equal(layer.bias_grad_sum,
                                       np.array([2, 4, 6])))
        self.assertTrue(np.array_equal(layer.weight_grad_sum,
                                       np.array([[2, 4],
                                                 [4, 8],
                                                 [6, 12]])))

    def test_gradient_descent(self):
        net = nn.StandardNetwork(1, 2, 3, 3, np.float32, 1, TestActivation)
        net._layers[0].weights = np.array([[1, 2],
                                          [3, 4],
                                          [5, 6]], dtype=np.float32)
        inputs = np.array([[1, 2]])
        labels = np.array([[8.5, 19, 29.5]])
        net.gradient_descent(inputs, labels, 2)
        layer = net._layers[0]
        self.assertTrue(np.array_equal(layer.biases,
                                       np.array([-4, -8, -12])))
        self.assertTrue(np.array_equal(layer.weights,
                                       np.array([[-3, -6],
                                                 [-5, -12],
                                                 [-7, -18]])))
        self.assertTrue(np.array_equal(layer.bias_grad_sum,
                                       np.array([0, 0, 0])))
        self.assertTrue(np.array_equal(layer.weight_grad_sum,
                                       np.array([[0, 0],
                                                 [0, 0],
                                                 [0, 0]])))
