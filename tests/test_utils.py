import unittest
import neural_net.util as util
import numpy as np


class Test(unittest.TestCase):
    def test_vector_transpose(self):
        actual = util.vector_transpose(np.array([1, 2, 3]))
        expected = np.array([[1],
                             [2],
                             [3]])
        self.assertTrue(np.array_equal(actual, expected))
