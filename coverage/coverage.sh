#!/bin/bash
set -e
coverage run --rcfile=coverage/.coveragerc
coverage report --rcfile=coverage/.coveragerc
coverage xml --rcfile=coverage/.coveragerc